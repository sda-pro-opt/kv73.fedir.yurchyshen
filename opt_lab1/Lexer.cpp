#include "Lexer.h"

std::vector<TokenInfo> Lexems;
std::vector<std::string> Error_log;
id_map TokenID;

ch_map initSymCat();

char NUM(std::ifstream& file, ch_map& SymbolCategories, char& symb, int& row, int& column);
char IDN(std::ifstream& file, ch_map& SymbolCategories, const std::set<std::string>& KeyWords, char& symb, int& row, int& column);
char DM2(std::ifstream& file, char& symb, int& row, int& column);
char BCOM(std::ifstream& file, char& symb, int& row, int& column);
char COM(std::ifstream& file, char& symb, int& row, int& column);
char ECOM(std::ifstream& file, char& symb, int& row, int& column);
char WS(std::ifstream& file, ch_map& SymbolCategories, char& symb, int& row, int& column);

void OUT_NUM(const std::string& temp, int& row, int& column);
void OUT_IDN(const std::set<std::string>& KeyWords, const std::string& temp, int& row, int& column);
void OUT_DM1(char& temp, int& row, int& column);
void OUT_DM2(const std::string& temp, int& row, int& column);
void ERR(char& symb, int row, int column, ErrorTypes err);


bool Lexer(const std::string& path_to_file) {
	std::ifstream file(path_to_file, std::ios::_Nocreate);

	if (file.is_open()) {
		ch_map SymbolCategories = initSymCat();
		std::set<std::string> KeyWords = {
		"PROGRAM", "BEGIN", "END", "LOOP", "ENDLOOP", "FOR", "ENDFOR", "TO", "DO"
		};

		char symb;
		int row = 1, column = 1;

		if (!file.get(symb)) symb = EOF;
		while (symb != EOF) {
			auto symb_cat = SymbolCategories[symb];

			switch (symb_cat) {
			case dig:
				symb = NUM(file, SymbolCategories, symb, row, column);
				break;
			case let:
				symb = IDN(file, SymbolCategories, KeyWords, symb, row, column);
				break;
			case dm1:
				OUT_DM1(symb, row, column);
				if (!file.get(symb)) symb = EOF;
				else column++;
				break;
			case dm2:
				symb = DM2(file, symb, row, column);
				break;
			case com:
				symb = BCOM(file, symb, row, column);
				break;
			case ws:
				symb = WS(file, SymbolCategories, symb, row, column);
				break;
			default:
				ERR(symb, row, column, ErrorTypes::ill_symb);
				if (!file.get(symb)) symb = EOF;
				else column++;
				break;
			}
		}
		file.close();
		return 1;
	}
	else return 0;
}


ch_map initSymCat() {
	ch_map SymbolCategories;

	for (int i = '0'; i <= '9'; i++) {
		SymbolCategories.emplace(i, Categories::dig);
	}

	for (int i = 'A'; i <= 'Z'; i++) {
		SymbolCategories.emplace(i, Categories::let);
	}

	SymbolCategories.emplace('+', Categories::dm1);
	SymbolCategories.emplace('-', Categories::dm1);
	SymbolCategories.emplace(';', Categories::dm1);
	SymbolCategories.emplace('.', Categories::dm1);

	SymbolCategories.emplace(':', Categories::dm2);

	SymbolCategories.emplace('(', Categories::com);

	for (int i = 8; i <= 13; i++) {
		SymbolCategories.emplace(i, Categories::ws);
	}
	SymbolCategories.emplace(' ', Categories::ws);

	return SymbolCategories;
}

char LIN(std::ifstream& file, ch_map& SymbolCategories, char& symb, int& row, int& column) {
	auto symb_category = Categories::let;

	column++;
	ERR(symb, row, column, ErrorTypes::let_in_num);
	if (!file.get()) symb = EOF;
	else {
		symb_category = SymbolCategories[symb];

		while (symb_category == Categories::let || symb_category == Categories::dig) {
			column++;
			if (file.get(symb)) {
				symb_category = SymbolCategories[symb];
			}
			else {
				symb = EOF;
				break;
			}
		}
	}

	return symb;
}

char NUM(std::ifstream& file, ch_map& SymbolCategories, char& symb, int& row, int& column) {
	std::string temp;
	int cur_col = column;

	auto symb_category = Categories::dig;

	while (symb_category == Categories::dig) {
		column++;
		temp += symb;
		if (file.get(symb)) {
			symb_category = SymbolCategories[symb];
		}
		else {
			symb = EOF;
			break;
		}
	}

	if (symb_category == Categories::let) symb = LIN(file, SymbolCategories, symb, row, column);
	else OUT_NUM(temp, row, cur_col);

	return symb;
}

char IDN(std::ifstream& file, ch_map& SymbolCategories, const std::set<std::string>& KeyWords, char& symb, int& row, int& column) {
	std::string temp;
	int cur_col = column;

	auto symb_category = Categories::let;

	while (symb_category == Categories::let || symb_category == Categories::dig) {
		column++;
		temp += symb;
		if (file.get(symb)) {
			symb_category = SymbolCategories[symb];
		}
		else {
			symb = EOF;
			break;
		}
	}
	OUT_IDN(KeyWords, temp, row, cur_col);

	return symb;
}

char DM2(std::ifstream& file, char& symb, int& row, int& column) {
	std::string temp;
	int cur_col = column;

	char input = symb;
	temp += symb;

	if (file.get(symb)) {
		column++;
		if (symb == '=') {
			temp += symb;
			OUT_DM2(temp, row, cur_col);
			if (!file.get(symb)) symb = EOF;
			else column++;
		}
		else ERR(input, row, column - 1, ErrorTypes::ill_symb);
	}
	else {
		ERR(input, row, column, ErrorTypes::ill_symb);
		symb = EOF;
	}

	return symb;
}

char BCOM(std::ifstream& file, char& symb, int& row, int& column) {
	if (file.get(symb)) {
		column++;
		if (symb == '*') return COM(file, symb, row, column);
		else { 
			ERR(symb, row, column, ErrorTypes::ill_symb);
			return symb;
		}
	}
	else {
		ERR(symb, row, column, ErrorTypes::ill_symb);
		return EOF;
	}
}
char COM(std::ifstream& file, char& symb, int& row, int& column) {
	if (symb == '\n') {
		row++;
		column = 0;
	}

	if (file.get(symb)) {
		column++;
		if (symb == '*') return ECOM(file, symb, row, column);
		else {
			while (symb != '*')
				if (file.get(symb)) {
					if (symb == '\n') {
						row++;
						column = 0;
					}
					else column++;
				}
				else {
					ERR(symb, row, column, ErrorTypes::uncl_com);
					return EOF;
				}
			return ECOM(file, symb, row, column);
		}	
	}
	else {
		ERR(symb, row, column, ErrorTypes::uncl_com);
		return EOF;
	}
}
char ECOM(std::ifstream& file, char& symb, int& row, int& column) {
	if (file.get(symb)) {
		column++;
		if (symb == '*') {
			while (symb == '*')
				if (file.get(symb)) column++;
				else {
					ERR(symb, row, column, ErrorTypes::uncl_com);
					return EOF;
				}
			if (symb == ')') {
				if (file.get(symb)) {
					column++;
					return symb;
				}
				else return EOF;
			}
			else return COM(file, symb, row, column);
		}
		else if (symb == ')') {
			if (file.get(symb)) {
				column++;
				return symb;
			}
			else return EOF;
		}
		else return COM(file, symb, row, column);
	}
	else {
		ERR(symb, row, column, ErrorTypes::uncl_com);
		return EOF;
	}
}

char WS(std::ifstream& file, ch_map& SymbolCategories, char& symb, int& row, int& column) {
	auto symb_category = Categories::ws;

	if (symb == '\n') {
		row++;
		column = 0;
	}

	while (symb_category == Categories::ws) {
		if (file.get(symb)) {
			if (symb == '\n') {
				row++;
				column = 0;
			}
			else column++;
			symb_category = SymbolCategories[symb];
		}
		else {
			symb = EOF;
			break;
		}
	}

	return symb;
}



void OUT_NUM(const std::string& temp, int& row, int& column) {
	static int id = 501;

	auto it = TokenID.emplace(temp, id);
	if (it.second) {
		Lexems.push_back(TokenInfo(id, temp, row, column));
		id++;
	}
	else Lexems.push_back(TokenInfo((*it.first).second, temp, row, column));
}

void OUT_KeyWord(const std::string& temp, int& row, int& column) {
	static int id = 401;
	auto it = TokenID.emplace(temp, id);
	if (it.second) {
		Lexems.push_back(TokenInfo(id, temp, row, column));
		id++;
	}
	else Lexems.push_back(TokenInfo((*it.first).second, temp, row, column));
}

void OUT_IDN(const std::set<std::string>& KeyWords, const std::string& temp, int& row, int& column) {
	static int id = 1001;

	if (KeyWords.find(temp) != KeyWords.end()) {
		OUT_KeyWord(temp, row, column);
	}
	else {
		auto it = TokenID.emplace(temp, id);
		if (it.second) {
			Lexems.push_back(TokenInfo(id, temp, row, column));
			id++;
		}
		else Lexems.push_back(TokenInfo((*it.first).second, temp, row, column));
	}
}

void OUT_DM1(char& temp, int& row, int& column) {
	std::string str; str = temp;
	Lexems.push_back(TokenInfo((int)temp, str, row, column));
}

void OUT_DM2(const std::string& temp, int& row, int& column) {
	static int id = 301;
	
	auto it = TokenID.emplace(temp, id);
	if (it.second) {
		Lexems.push_back(TokenInfo(id, temp, row, column));
		id++;
	}
	else Lexems.push_back(TokenInfo((*it.first).second, temp, row, column));
}

void ERR(char& symb, int row, int column, ErrorTypes err) {
	switch (err) {
	case uncl_com:
		Error_log.push_back("Lexer: Error (line " + std::to_string(row) + ", column " + std::to_string(column) + "): Unclosed comment\n");
		break;
	case ill_symb:
		Error_log.push_back("Lexer: Error (line " + std::to_string(row) + ", column " + std::to_string(column) + "): Illegal symbol '" + symb + "'\n");
		break;
	case let_in_num:
		Error_log.push_back("Lexer: Error (line " + std::to_string(row) + ", column " + std::to_string(column) + "): Leter '" + symb + "' in number\n");
		break;
	default:
		break;
	}
}



std::vector<TokenInfo>& getLexems() {
	return Lexems;
}
std::vector<std::string>& getErrLog() {
	return Error_log;
}
id_map& getTokenID() {
	return TokenID;
}
