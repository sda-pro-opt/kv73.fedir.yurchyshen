#pragma once
#include <string>

class TokenInfo {
public:
	TokenInfo(int id, std::string lexem, int row, int column);
	int getID();
	std::string getLexem();
	int getRow();
	int getCol();
private:
	int id;
	std::string lexem;
	int row;
	int column;
};

