#include "Lexer.h"
#include "Print.h"

int main() {
	std::string path_to_folder("tests\\test01\\");
	std::string infilename("input.sig");
	std::string outfilename("generated.txt");

	if (Lexer(path_to_folder + infilename)) {
		ClearFile(path_to_folder + outfilename);

		PrintFile(path_to_folder, infilename, outfilename);
		PrintLexems(path_to_folder, outfilename, getLexems());
		PrintErrLog(path_to_folder, outfilename, getErrLog());
		PrintTokenID(path_to_folder, outfilename, getTokenID());

		std::cout << "Output file '" << path_to_folder << outfilename << "' was created" << std::endl;
	}
	else {
		std::cout << "Error opening file '"<< path_to_folder << infilename << "'" << std::endl;
	}

	return 0;
}
