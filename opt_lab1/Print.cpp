#include "Print.h"

void ClearFile(const std::string& path_to_file) {
	std::ofstream file(path_to_file);
	file.close();
}

void PrintFile(const std::string& path_to_folder, const std::string& infilename, const std::string& outfilename) {
std::ifstream file(path_to_folder + infilename);
	std::ofstream fout(path_to_folder + outfilename, std::ios::app);
	std::string line; int i = 1;

	fout << "File contains:" << std::endl;
	while (std::getline(file, line)) {
		fout << std::setw(3) << i;
		fout << '|' << line << std::endl;
		i++;
	}
	fout << std::endl << std::endl;

	file.close();
	fout.close();
}

void PrintLexems(const std::string& path_to_folder, const std::string& outfilename, std::vector<TokenInfo>& Lexems) {
int id, row, column;
	std::string lexem;

	std::ofstream fout(path_to_folder + outfilename, std::ios::app);

	fout << "row:\tcol:\tid:\tlexem:\n";
	for (auto& el : Lexems) {
		id = el.getID();
		lexem = el.getLexem();
		row = el.getRow();
		column = el.getCol();

		fout << row << '\t' << column << '\t' << id << '\t' << lexem << std::endl;
	}
	fout << std::endl << std::endl;

	fout.close();
}

void PrintErrLog(const std::string& path_to_folder, const std::string& outfilename, std::vector<std::string>& Error_log) {
std::ofstream fout(path_to_folder + outfilename, std::ios::app);

	fout << "Lexical errors: " << Error_log.size() << std::endl;
	
	for (auto& str : Error_log) {
		fout << str;
	}
	fout << std::endl << std::endl;

	fout.close();
}

void PrintTokenID(const std::string& path_to_folder, const std::string& outfilename, std::map<std::string, int>& TokenID) {
	std::ofstream fout(path_to_folder + outfilename, std::ios::app);

	fout << "Token table:" << std::endl;

	for (auto token : TokenID) {
		fout << token.second << '\t' << token.first << std::endl;
	}
	fout << std::endl;

	fout.close();
}