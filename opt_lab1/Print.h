#pragma once
#include "TokenInfo.h"
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <map>
#include <iomanip>


void ClearFile(const std::string& path_to_file);

void PrintFile(const std::string& path_to_folder, const std::string& infilename , const std::string& outfilename);

void PrintLexems(const std::string& path_to_folder, const std::string& outfilename, std::vector<TokenInfo>& Lexems);

void PrintErrLog(const std::string& path_to_folder, const std::string& outfilename, std::vector<std::string>& Error_log);

void PrintTokenID(const std::string& path_to_folder, const std::string& outfilename, std::map<std::string, int>& TokenID);