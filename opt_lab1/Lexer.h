#pragma once
#include "TokenInfo.h"
#include <string>
#include <algorithm>
#include <map>
#include <set>
#include <vector>
#include <iostream>
#include <fstream>

enum Categories {
	dig = 1, let, dm1, dm2, com, ws
};

enum ErrorTypes {
	uncl_com, ill_symb, let_in_num
};

typedef std::pair<char, Categories> pair;
typedef std::map<char, Categories> ch_map;
typedef std::map<std::string, int> id_map;


bool Lexer(const std::string& path_tofile);


std::vector<TokenInfo>& getLexems();
std::vector<std::string>& getErrLog();
id_map& getTokenID();



