#include "TokenInfo.h"

TokenInfo::TokenInfo(int id, std::string lexem, int row, int column) {
	this->id = id;
	this->lexem = lexem;
	this->row = row;
	this->column = column;
}

int TokenInfo::getID() {
	return this->id;
}

std::string TokenInfo::getLexem() {
	return this->lexem;
}

int TokenInfo::getRow() {
	return this->row;
}

int TokenInfo::getCol() {
	return this->column;
}